# Требования
Версия php 7.4

## Установка

1. Клонировать проект
2. Переименовать файл .env.example в .env
3. Прописать ссылку проекта в файле .env`
```
APP_URL=http://u-shrt.lc
```
4. Создать базу данных и прописать конфигурацию в файле .env`
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=url_shortener
DB_USERNAME=root
DB_PASSWORD=
```
5. Установить драйвер очереди в файле .env `
```
QUEUE_CONNECTION=database
```
6. Запустить команду`
```bash
 php artisan migrate
```
7. Прописать в файле .env настройки почтового ящика для рассылки или использовать эти`
```bash
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=7e3374e99d2e6e
MAIL_PASSWORD=732ada7b7e9264
MAIL_ENCRYPTION=null
```
доступы к аккаунту mailtrap login-webhovhannisyan.alex@gmail.com, password-12345678, чтобы просмотреть письма
8. Для запуска очереди использовать команду
```bash
php artisan queue:work
```
конфигурации супервизора нет, поскольку разработка велась на windows

