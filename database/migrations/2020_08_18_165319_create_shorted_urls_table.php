<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShortedUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shorted_urls', function (Blueprint $table) {
            $table->id();
            $table->string('shorted_key')
                ->nullable(false)
                ->unique();
            $table->string('search_key')
                ->nullable(false)
                ->unique();
            $table->text('original_url')
                ->nullable(false);
            $table->timestamps();

            $table->index('shorted_key');
            $table->index('search_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shorted_urls');
    }
}
