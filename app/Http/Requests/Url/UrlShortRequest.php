<?php

namespace App\Http\Requests\Url;

use App\Http\Requests\BaseApiRequest;
use App\Rules\Url\SiteUrl;

class UrlShortRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'original_url' => new SiteUrl()
        ];
    }

}
