<?php

namespace App\Http\Controllers;

use App\Http\Requests\Url\UrlShortRequest;
use App\Jobs\Email\ShortedJob;
use App\Models\ShortedUrls;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;


class UrlController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * url shortener page
     *
     * @return View
     */
    public function index()
    {
        return view('url.index');
    }


    /**
     * short given url and save in database
     *
     * @param UrlShortRequest $request
     * @return JsonResponse
     */
    public function short(UrlShortRequest $request)
    {
        $original = $request->original_url;
        $searchKey = md5($original);
        $shorted = ShortedUrls::findOrNewByKey($searchKey, $original);
        $response = [
            'shorted_url' => $shorted['shorted']->full_shorted_url,
            'notify' => ''
        ];
        if(!$shorted['new']){
            $response['notify'] = 'This url has been already shorted';
        }
        return response()->json([
            $response
        ],200);
    }


    /**
     * redirect to original by shorted
     *
     * @param ShortedUrls $shorted_url
     * @return RedirectResponse
     */
    public function redirect(ShortedUrls $shorted_url)
    {
        return redirect($shorted_url->original_url);
    }


}
