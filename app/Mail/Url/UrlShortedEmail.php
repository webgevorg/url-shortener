<?php

namespace App\Mail\Url;

use App\Models\ShortedUrls;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UrlShortedEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected ShortedUrls $url;

    /**
     * Create a new message instance.
     *
     * @param ShortedUrls $url
     */
    public function __construct(ShortedUrls $url)
    {
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Url has been shorted');
        return $this->view('emails.url.shorted')
            ->with(['shorted' => $this->url]);
    }
}
