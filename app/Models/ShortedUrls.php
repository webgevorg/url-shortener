<?php

namespace App\Models;

use App\Mail\Url\UrlShortedEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

/**
 * @property string $search_key
 * @property string $shorted_key
 * @property string $original_url
 * @property string $full_shorted_url
 */
class ShortedUrls extends Model
{

    /**
     * @inheritDoc
     */
    public function getRouteKeyName(): string
    {
        return 'shorted_key';
    }

    /**
     * find shorted in database or create new object
     *
     * @param string $key
     * @param string $original
     * @return array
     */
    public static function findOrNewByKey($key, $original): array
    {
        if (self::checkExisting($key)) {
            return [
                'new' => false,
                'shorted' => self::where(['search_key' => $key])->first()
            ];
        }
        $new = new self();
        $new->search_key = $key;
        $new->shorted_key = uniqid();
        $new->original_url = $original;
        $new->save();
        Mail::to(auth()->user()->email)->send(new UrlShortedEmail($new));
        return [
            'new' => true,
            'shorted' => $new
        ];
    }

    /**
     * check if that url shorted
     *
     * @param string $key
     * @return bool
     */
    protected static function checkExisting($key): bool
    {
        return self::where(['search_key' => $key])->exists();
    }


    /**
     * this attribute returns full shorted url
     *
     * @return string
     */
    public function getFullShortedUrlAttribute(): string
    {
        return env('APP_URL') . "/{$this->shorted_key}";
    }
}
