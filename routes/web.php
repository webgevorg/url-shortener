<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('short.index'));
});

Route::get('/short', 'UrlController@index')->name('short.index');
Route::get('/url/short', 'UrlController@short');
Auth::routes();


/**
 * it should be at the very end of the routes so
 * as not to interfere with the work of the others
 */
Route::get('/{shorted_url}', 'UrlController@redirect');




